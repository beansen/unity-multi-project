﻿Shader "Custom/Terrain" {
	Properties {
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		const static int maxColorCount = 8;

		int baseColorCount;
		float3 baseColors[maxColorCount];
		float baseStartHeights[maxColorCount];

        float minHeight;
        float maxHeight;

        struct Input {
        	float3 worldPos;
        };

        float inverseLerp(float min, float max, float value) {
            return saturate((value-min) / (max-min));
        }

		void surf (Input IN, inout SurfaceOutputStandard o) {
			float heightColor = inverseLerp(minHeight, maxHeight, IN.worldPos.y);
			for (int i = 0; i < baseColorCount; i++) {
				float strength = saturate(sign(heightColor - baseStartHeights[i]));
				o.Albedo = o.Albedo * (1-strength) + baseColors[i] * strength;
			}
		}
		ENDCG
	}
	FallBack "Diffuse"
}
