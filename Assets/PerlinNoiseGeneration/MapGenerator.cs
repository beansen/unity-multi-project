﻿using UnityEngine;

public class MapGenerator : MonoBehaviour
{

	public AnimationCurve heightCurve;

	public Color[] baseColors;
	[Range(0, 1)]
	public float[] baseHeights;
	private int width = 200;
	private int height = 200;

	private float distance = 0.5f;

	private float scale = 50f;
	private int octaves = 3;
	private float persistency = 0.2f;
	private float lacunarity = 1.5f;

	private float terrainHeight = 20f;

	private MapDataCreator mapDataCreator;

	private float timer;

	private int seed = 1234;

	private int steps = 2;
	private int currentStep = 0;

	private Vector2 initialOffset;

	private Vector3 currentPosition;

	private int direction = -1;

	// Use this for initialization
	void Start () {
		Random.InitState(seed);
		currentPosition = new Vector3(width / 2, 0, height / 2);
		mapDataCreator = new MapDataCreator(heightCurve);
		initialOffset = new Vector2(Random.Range(-1000f, 1000f), Random.Range(-1000f, 1000f));
		CreatePatch(currentPosition);
		currentPosition.x += height;
		CreatePatch(currentPosition);
	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		timer += Time.fixedDeltaTime;

		if (timer >= 4)
		{
			if (direction == -1)
			{
				currentPosition.x += width;
				currentPosition.z -= height;
				direction = 0;
				return;
			}

			if (direction == 0)
			{
				currentPosition.z += height;
				currentStep++;

				if (currentStep == steps)
				{
					direction++;
					currentStep = 0;
				}
			} else if (direction == 1)
			{
				currentPosition.x -= width;
				currentStep++;

				if (currentStep == steps)
				{
					direction++;
					currentStep = 0;
				}
			} else if (direction == 2)
			{
				currentPosition.z -= height;
				currentStep++;

				if (currentStep == steps)
				{
					direction++;
					currentStep = 0;
				}
			}
			else
			{
				currentPosition.x += width;
				currentStep++;

				if (currentStep == steps)
				{
					direction = -1;
					currentStep = 0;
					steps += 2;
				}
			}

			CreatePatch(currentPosition);
			timer = 0;
		}
	}

	private void CreatePatch(Vector3 position)
	{
		Vector2 coordinates = GetCoordinates(position);
		Vector2 offset = new Vector2(initialOffset.x + (width - 1) * coordinates.x, initialOffset.y + (height - 1) * coordinates.y);
		MapDataCreator.MapData data = mapDataCreator.GetData(width, height, distance, scale, octaves, persistency, lacunarity,
			terrainHeight, offset);

		Vector3 patchPosition = new Vector3(coordinates.x * (width - 1) * distance, 0, coordinates.y * (height - 1) * distance);
		GameObject container = new GameObject();
		MeshFilter meshFilter = container.AddComponent<MeshFilter>();
		MeshRenderer meshRenderer = container.AddComponent<MeshRenderer>();
		meshRenderer.material = new Material(Shader.Find("Custom/Terrain"));

		container.transform.position = patchPosition;
		meshFilter.mesh = data.mesh;

		meshRenderer.material.SetFloat("minHeight", data.minHeight - 0.1f);
		meshRenderer.material.SetFloat("maxHeight", data.maxHeight);
		meshRenderer.material.SetInt("baseColorCount", baseColors.Length);
		meshRenderer.material.SetColorArray("baseColors", baseColors);
		meshRenderer.material.SetFloatArray("baseStartHeights", baseHeights);
	}

	private Vector2 GetCoordinates(Vector3 position)
	{
		int x = (int) (position.x / width);
		int y = (int) (position.z / height);

		if (position.x < 0)
			x--;
		if (position.z < 0)
			y--;

		return new Vector2(x, y);
	}
}
