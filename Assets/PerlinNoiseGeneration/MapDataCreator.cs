﻿﻿
using System.Collections.Generic;
using UnityEngine;

public class MapDataCreator
{

	private List<Vector3> verticesAll = new List<Vector3>();
	private List<int> trianglesAll = new List<int>();

	private AnimationCurve heightCurve;

	public MapDataCreator(AnimationCurve heightCurve)
	{
		this.heightCurve = heightCurve;
	}


	public MapData GetData(int width, int height, float distance, float scale, int octaves, float persistency, float lacunarity, float terrainHeight, Vector2 offset)
	{
		if (scale == 0)
		{
			scale = 0.001f;
		}

		Vector3[] vertices = new Vector3[width * height];
		int[] triangles = new int[(width - 1) * (height - 1) * 6];
		Vector2[] uv = new Vector2[width * height];

		int verticesCount = 0;
		int trianglesCount = 0;

		float minHeight = float.MaxValue;
		float maxHeight = 0;

		for (int z = -1; z < height + 1; z++)
		{
			for (int x = -1; x < width + 1; x++)
			{
				float y = GetHeightMultiplier(x, z, width, height, scale, octaves, persistency, lacunarity, offset);

				y = heightCurve.Evaluate(y);

				if (y < minHeight)
				{
					minHeight = y;
				}

				if (y > maxHeight)
				{
					maxHeight = y;
				}

				Vector3 vertex = new Vector3(x * distance, y * terrainHeight, z * distance);

				verticesAll.Add(vertex);

				if (x < width && z < height)
				{
					trianglesAll.Add((z + 1) * (width + 2) + (x + 1));
					trianglesAll.Add((z + 2) * (width + 2) + (x + 1));
					trianglesAll.Add((z + 1) * (width + 2) + (x + 2));
					trianglesAll.Add((z + 2) * (width + 2) + (x + 1));
					trianglesAll.Add((z + 2) * (width + 2) + (x + 2));
					trianglesAll.Add((z + 1) * (width + 2) + (x + 2));
				}

				if (x >= 0 && x < width && z >= 0 && z < height)
				{
					vertices[verticesCount] = vertex;
					uv[verticesCount] = new Vector2(x / (float) width, z / (float) height);
					verticesCount++;
				}

				if (x < width - 1 && z < height - 1 && x >= 0 && z >= 0)
				{
					triangles[trianglesCount++] = z * width + x;
					triangles[trianglesCount++] = (z + 1) * width + x;
					triangles[trianglesCount++] = z * width + x + 1;
					triangles[trianglesCount++] = (z + 1) * width + x;
					triangles[trianglesCount++] = (z + 1) * width + x + 1;
					triangles[trianglesCount++] = z * width + x + 1;
				}
			}
		}

		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = uv;
		mesh.normals = GetNormals(width, height, distance);

		verticesAll.Clear();
		trianglesAll.Clear();

		return new MapData(mesh, minHeight * terrainHeight, maxHeight * terrainHeight);
	}

	private float GetHeightMultiplier(int x, int z, int width, int height, float scale, int octaves, float persistency, float lacunarity, Vector2 offset)
	{
		float y = 0;
		float amplitude = 1f;
		float frequency = 1;

		int xCoord = x;
		int zCoord = z;

		float xOffset = offset.x;
		float zOffset = offset.y;

		if (x == -1)
		{
			xCoord = width - 1;
			xOffset -= width - 1;
		}

		if (x == width)
		{
			xCoord = 0;
			xOffset += width - 1;
		}

		if (z == -1)
		{
			zCoord = height - 1;
			zOffset -= height - 1;
		}

		if (z == height)
		{
			zCoord = 0;
			zOffset += height - 1;
		}

		for (int i = 0; i < octaves; i++)
		{
			float sampleX = (xCoord + xOffset) / scale * frequency;
			float sampleZ = (zCoord + zOffset) / scale * frequency;

			float perlinValue = Mathf.PerlinNoise(sampleX, sampleZ) * 2 - 1;
			y += perlinValue * amplitude;

			amplitude *= persistency;
			frequency *= lacunarity;
		}

		return y;
	}

	private Vector3[] GetNormals(int width, int height, float distance)
	{
		Vector3[] points = new Vector3[3];

		int normalsAmount = verticesAll.Count - 2 * (width + 2) - 2 * height;
		Vector3[] normals = new Vector3[normalsAmount];

		for (int i = 0; i < trianglesAll.Count; i += 6)
		{
			int vertexIndexA = trianglesAll[i];
			int vertexIndexB = trianglesAll[i + 1];
			int vertexIndexC = trianglesAll[i + 2];

			points[0] = verticesAll[vertexIndexA];
			points[1] = verticesAll[vertexIndexB];
			points[2] = verticesAll[vertexIndexC];

			Vector3 sideA = points[2] - points[0];
			Vector3 sideB = points[1] - points[0];
			Vector3 cross = Vector3.Cross(sideB, sideA).normalized;

			for (int k = 0; k < points.Length; k++)
			{
				if (IsInMesh(points[k], width * distance, height * distance))
				{
					int index = GetRealIndex(trianglesAll[i + k], width);
					normals[index] += cross;
				}
			}

			vertexIndexA = trianglesAll[i + 3];
			vertexIndexB = trianglesAll[i + 4];
			vertexIndexC = trianglesAll[i + 5];

			points[0] = verticesAll[vertexIndexA];
			points[1] = verticesAll[vertexIndexB];
			points[2] = verticesAll[vertexIndexC];

			sideA = points[0] - points[1];
			sideB = points[2] - points[1];
			cross = Vector3.Cross(sideB, sideA).normalized;

			for (int k = 0; k < points.Length; k++)
			{
				if (IsInMesh(points[k], width * distance, height * distance))
				{
					int index = GetRealIndex(trianglesAll[i + k + 3], width);
					normals[index] += cross;
				}
			}
		}

		for (int i = 0; i < normals.Length; i++)
		{
			normals[i].Normalize();
		}

		return normals;
	}

	private int GetRealIndex(int index, int width)
	{
		int row = index / (width + 2);
		int realIndex = index - (width + 2) - (row - 1) * 2 - 1;
		return realIndex;
	}

	private bool IsInMesh(Vector3 point, float width, float height)
	{
		return point.x >= 0 && point.x < width && point.z >= 0 && point.z < height;
	}

	public struct MapData
	{
		public Mesh mesh;
		public float minHeight;
		public float maxHeight;

		public MapData(Mesh mesh, float minHeight, float maxHeight)
		{
			this.mesh = mesh;
			this.minHeight = minHeight;
			this.maxHeight = maxHeight;
		}
	}
}
