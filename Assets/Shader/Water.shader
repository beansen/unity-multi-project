﻿Shader "Custom/Water" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Height ("Height", Range(0, 1)) = 0
		_Wave ("Wave", Range(0, 1)) = 0
	}

	SubShader {
		Pass {
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma vertex vert
		#pragma fragment frag

		struct Input {
			float2 uv_MainTex : TEXCOORD0;
			float4 position : POSITION;
		};

		struct v2f {
			float4 position : SV_POSITION;
			float2 uv_MainTex : TEXCOORD0;
		};

		fixed4 _Color;
		sampler2D _MainTex;
		float _Height;
		float _Wave;

		//float4 _MainTex_ST;

		v2f vert (Input IN)
    	{
    		v2f OUT;
    		OUT.position = mul(UNITY_MATRIX_MVP, IN.position);
    		OUT.position.y += _Height + sin(_Time.y) * _Wave / 20;
    		OUT.uv_MainTex = IN.uv_MainTex;
    		OUT.uv_MainTex += sin(_Time.z) / 100;

    		return OUT;
    	}

    	fixed4 frag(v2f IN) : SV_Target 
    	{
    		float4 textureColor = tex2D(_MainTex, IN.uv_MainTex);

    		return textureColor * _Color;
    	}

		ENDCG
		}
	}

	//FallBack "Diffuse"
}
