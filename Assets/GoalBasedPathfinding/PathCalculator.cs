﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathCalculator : MonoBehaviour
{
	public int goalX;
	public int goalY;

	public int particlesAmount;

	public GameObject numbersPrefab;

	private int width = 50;
	private int height = 50;

	private GoalBasedPathfinder pathfinder;

	private bool move;

	private List<Particle> particles;

	// Use this for initialization
	void Start () {
		pathfinder = new GoalBasedPathfinder(width, height);
		particles = new List<Particle>();
		CreateParticles();
		CreateObstacles();
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < particles.Count; i++)
		{
			Particle particle = particles[i];
			Vector3 pos = particle.GetCurrentPosition();
			particle.SetDirection(pathfinder.GetDirectionAt((int) pos.x, (int) pos.z));
			particle.Move();
		}
	}

	public void UpdateGoal()
	{
		pathfinder.SetGoal(goalX, goalY);
	}

	private void CreateNumbers()
	{
		bool createNew = false;

		GameObject numbersContainer = GameObject.Find("Numbers");

		if (numbersContainer == null)
		{
			numbersContainer = new GameObject("Numbers");
			createNew = true;
		}

		int childCount = 0;

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				if (pathfinder.GetValueAt(x, y) != pathfinder.GetObstacleValue())
				{
					GameObject number;

					if (createNew)
					{
						number = Instantiate(numbersPrefab);
						number.transform.position = new Vector3(x + 0.01f, 0.1f, y + 1);
						number.transform.SetParent(numbersContainer.transform);

					}
					else
					{
						number = numbersContainer.transform.GetChild(childCount).gameObject;
						childCount++;
					}

					number.GetComponent<TextMesh>().text = pathfinder.GetValueAt(x, y).ToString();
				}
			}
		}
	}

	private void CreateParticles()
	{
		GameObject particlesContainer = new GameObject("Particles");

		for (int i = 0; i < particlesAmount; i++)
		{
			GameObject sphereObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			float xValue = Random.Range(0f, width);
			float zValue = Random.Range(0f, height);
			sphereObject.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
			sphereObject.transform.position = new Vector3(xValue, 0.1f, zValue);
			sphereObject.transform.SetParent(particlesContainer.transform);
			particles.Add(new Particle(sphereObject.transform));
		}
	}

	private void CreateObstacles()
	{
		Vector3 point = new Vector3(0, 2, 0);

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				point.x = x + 0.5f;
				point.z = y + 0.5f;

				RaycastHit hit;

				if (Physics.Raycast(point, Vector3.down, out hit))
				{
					if (hit.transform.name.Contains("Cube"))
					{
						pathfinder.SetObstacle(x, y);
					}
				}
			}
		}
	}

	void OnDrawGizmos()
	{
		if (move)
		{
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					Vector3 middle = new Vector3(x + 0.5f, 0.2f, y + 0.5f);
					Vector3 direction = pathfinder.GetDirectionAt(x, y);
					Vector3 end = middle + direction;
					Gizmos.DrawLine(middle, end);
				}
			}
		}
	}

	private class Particle
	{
		private Transform particleTransform;
		private Vector3 direction;

		public Particle(Transform particleTransform)
		{
			this.particleTransform = particleTransform;
		}

		public void SetDirection(Vector3 direction)
		{
			if (direction != Vector3.zero && this.direction != direction)
			{
				this.direction = direction;
			}
		}

		public void Move()
		{
			particleTransform.position += direction * Time.deltaTime;
		}

		public Vector3 GetCurrentPosition()
		{
			return particleTransform.position;
		}
	}
}
