﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class GoalBasedPathfinder {

	private Node[,] nodes;

	private Queue<int[]> queue;

	private Dictionary<int, Node> map;
	private Dictionary<int, Node> obstacles;

	private int obstacle;

	private int width;
	private int height;

	private int goalX;
	private int goalY;

	private bool syncing;

	private Thread heatmapThread;

	public GoalBasedPathfinder(int width, int height)
	{
		this.width = width;
		this.height = height;

		nodes = new Node[width,height];

		map = new Dictionary<int, Node>();
		obstacles = new Dictionary<int, Node>();

		obstacle = width * height;

		queue = new Queue<int[]>();
	}

	public void SetGoal(int x, int y)
	{
		goalX = x;
		goalY = y;

		if (heatmapThread == null || heatmapThread.ThreadState == ThreadState.Stopped) {
			heatmapThread = new Thread(new ThreadStart(() => {
				GenerateHeatmap();
				syncing = true;
				GenerateVectorField();
				syncing = false;
			}));

			heatmapThread.Start();
		}
	}

	public void SetObstacle(int x, int y)
	{
		Node node = new Node();
		node.SetValue(obstacle);
		obstacles.Add(x * width + y, node);
	}

	public Vector3 GetDirectionAt(int x, int y)
	{
		if (syncing || nodes[x, y] == null) {
			return Vector3.zero;
		}

		return nodes[x, y].GetDirection();
	}

	public int GetValueAt(int x, int y)
	{
		return nodes[x, y].GetValue();
	}

	public int GetObstacleValue()
	{
		return obstacle;
	}

	private void GenerateHeatmap()
	{
		queue.Enqueue(new []{goalX, goalY});
		map.Add(goalX * width + goalY, new Node());

		while (queue.Count > 0)
		{
			int[] pos = queue.Dequeue();

			int value = GetNode(pos[0], pos[1]).GetValue() + 1;

			if (pos[0] - 1 >= 0)
			{
				Node node = GetNode(pos[0] - 1, pos[1]);

				if (node.GetValue() == 0)
				{
					node.SetValue(value);
					queue.Enqueue(new []{pos[0] - 1, pos[1]});
				}
			}

			if (pos[0] + 1 < width)
			{
				Node node = GetNode(pos[0] + 1, pos[1]);

				if (node.GetValue() == 0)
				{
					node.SetValue(value);
					queue.Enqueue(new []{pos[0] + 1, pos[1]});
				}
			}

			if (pos[1] - 1 >= 0)
			{
				Node node = GetNode(pos[0], pos[1] - 1);

				if (node.GetValue() == 0)
				{
					node.SetValue(value);
					queue.Enqueue(new []{pos[0], pos[1] - 1});
				}
			}

			if (pos[1] + 1 < height)
			{
				Node node = GetNode(pos[0], pos[1] + 1);

				if (node.GetValue() == 0)
				{
					node.SetValue(value);
					queue.Enqueue(new []{pos[0], pos[1] + 1});
				}
			}
		}
	}

	private void GenerateVectorField()
	{
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int key = x * width + y;

				Node node = GetNode(x, y);

				if (node.GetValue() != obstacle)
				{
					int left = node.GetValue();
					int right = node.GetValue();
					int top = node.GetValue();
					int bottom = node.GetValue();

					if (x - 1 >= 0)
					{
						Node leftNode = GetNode(x - 1, y);
						left = leftNode.GetValue() != obstacle ? leftNode.GetValue() : node.GetValue();
					}

					if (x + 1 < width)
					{
						Node rightNode = GetNode(x + 1, y);
						right = rightNode.GetValue() != obstacle ? rightNode.GetValue() : node.GetValue();
					}

					if (y - 1 >= 0)
					{
						Node bottomNode = GetNode(x, y - 1);
						bottom = bottomNode.GetValue() != obstacle ? bottomNode.GetValue() : node.GetValue();
					}

					if (y + 1 < height)
					{
						Node topNode = GetNode(x, y + 1);
						top = topNode.GetValue() != obstacle ? topNode.GetValue() : node.GetValue();
					}

					Vector3 direction = new Vector3(left - right, 0, bottom - top).normalized;

					if ((x - 1 >= 0 && left == node.GetValue()) || (x + 1 < width && right == node.GetValue()))
					{
						direction.x = 0;
					}
					else if ((y + 1 < height && top == node.GetValue()) || (y - 1 >= 0 && bottom == node.GetValue()))
					{
						direction.z = 0;
					}

					node.SetDirection(direction);
				}

				nodes[x, y] = node;
			}
		}

		map.Clear();
	}

	private Node GetNode(int x, int y)
	{
		int key = x * width + y;

		if (obstacles.ContainsKey(key)) {
			return obstacles[key];
		}

		if (map.ContainsKey(key)) {
			return map[key];
		} else {
			Node node = new Node();
			map.Add(key, node);
			return node;
		}
	}

	private class Node
	{
		private int value;
		private Vector3 direction;

		public void SetValue(int value)
		{
			this.value = value;
		}

		public int GetValue()
		{
			return value;
		}

		public void SetDirection(Vector3 direction)
		{
			this.direction = direction;
		}

		public Vector3 GetDirection()
		{
			return direction;
		}
	}
}
